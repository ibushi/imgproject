import * as React from 'react';
import ImageWrapper from './components/image';

const App: React.FC = () => {
  return (
    <>
      <header className="header">
        <h1>Upload an image and have fun!</h1>
      </header>
      <div className="body">
        <ImageWrapper />
      </div>
    </>
  );
};

export default App;


/*
1. Build React App in Typescript where you can upload an image and rotate it on the screen for the desired angle, turn on black&white, filter and calculate the number of pixels.
*/