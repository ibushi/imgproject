import React, { useState, createContext, useEffect, useReducer } from 'react';

import UploadImage from './upload';
import SlidersList from './sliders';
import ImageView from './view';
import { imageReducer } from './shared/reducer';
import { defaultState } from './shared/config';

export const Context = createContext({});

export default function Image(): JSX.Element {
  const [state, dispatch] = useReducer(imageReducer, defaultState);

  const [src, setSrc] = useState('');

  useEffect(() => {
    if (!src) dispatch({ type: 'RESET' });
  }, [src]);

  return (
    <Context.Provider value={{ dispatch, filters: state }}>
      <div className="container-fluid">
        <div className="row justify-content-md-center">
          <div className="col col-lg-10 col-xl-8">
            <UploadImage setSrc={setSrc} />
          </div>
        </div>
        <div className="row justify-content-md-center">
          <div className="col col-lg-10 col-xl-8">
            <ImageView src={src} />
          </div>
        </div>
        <div className="row justify-content-md-center">
          <div className="col col-lg-10 col-xl-8">
            {src && <SlidersList />}
          </div>
        </div>
      </div>
    </Context.Provider>
  );
}

