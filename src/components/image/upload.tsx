import React, { useState } from 'react';

import { imageConfig } from './shared/config';
import { AppToaster } from '../shared/toaster';
import { UploadImageProps } from './shared/types';

export default function UploadImage({ setSrc }: UploadImageProps): JSX.Element {
  const [file, setFile] = useState({ name: '' });

  const uploadFile = (e: React.ChangeEvent<HTMLInputElement>) => {
    const uploadedFiles: FileList | [] = e.target.files || [];
    const uploadedFile: Blob = uploadedFiles[0];

    if (!uploadedFile || !validateSize(uploadedFile.size) || !validateType(uploadedFile.type)) {
      setFile({ name: '' });
      setSrc('');
      return;
    }

    setFile(uploadedFile as File);

    // parse binary to base64
    const reader = new FileReader();
    reader.onload = (e: ProgressEvent | any) =>
      e && e.target ? setSrc(e.target.result) : '';
    reader.readAsDataURL(uploadedFile);
  };

  const validateType = (type: string) => {
    if (!type) return false;

    const typeSplitted = type.split('/');
    if (typeSplitted.includes('image')) return true;

    return false;
  };

  const validateSize = (size: number) => {
    const { maxSize, maxSizeMB } = imageConfig;

    if (maxSize < size) {
      AppToaster.show({ message: `Maximum allowed size for file is ${maxSizeMB} MB` });
      return false;
    }

    return true;
  };

  return (
    <div className="image__upload-wrapper">
      <label htmlFor="file">
        <span className="image__upload-button">Upload image</span>
        <span className="image__file-name">{file ? file.name : ''}</span>
      </label>
      <input id="file" className="input-file" type="file" onChange={uploadFile} />
    </div>
  );
}
