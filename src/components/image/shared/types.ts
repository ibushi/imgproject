import { SetStateAction, Dispatch } from 'react';

// Components
export interface UploadImageProps {
  setSrc: Dispatch<SetStateAction<string>>;
}

export interface ImageViewProps {
  src: string;
}

export interface SliderItemProps {
  type: string;
  value: number;
}

// Config
export interface ImageConfigProps {
  maxSize: number;
  maxSizeMB: number;
}

export interface FiltersConfigProps {
  [filter: string]: {
    label: string;
    min: number;
    max: number;
    sufix: string;
    labelStepSize?: number;
  };
}

export interface DefaultStateProps {
  [filter: string]: number;
}

// Reducer
export interface ReducerActionProps {
  type: string;
  payload?: SliderItemProps;
}
