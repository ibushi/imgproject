import { defaultState } from './config';
import { FiltersConfigProps, ReducerActionProps } from './types';

export function imageReducer(state: FiltersConfigProps, action: ReducerActionProps) {
  switch (action.type) {
    case 'CHANGE': {
      return {
        ...state,
        [action.payload!.type]: action.payload!.value
      };
    }
    case 'RESET': {
      return defaultState;
    }
    default: {
      return state;
    }
  }
}
