import { ImageConfigProps, FiltersConfigProps, DefaultStateProps } from './types';

export const imageConfig: ImageConfigProps = {
  maxSize: 1 * 1024 * 1024, // bytes
  maxSizeMB: 1, // MB
};

export const slidersConfig: FiltersConfigProps = {
  rotate: {
    label: 'Rotation',
    min: 0,
    max: 360,
    sufix: 'deg',
    labelStepSize: 30
  },
  grayscale: {
    label: 'Grayscale',
    min: 0,
    max: 100,
    sufix: '%'
  },
  blur: {
    label: 'Blur',
    min: 0,
    max: 50,
    sufix: 'px',
    labelStepSize: 5,
  },
  brightness: {
    label: 'Brightness',
    min: 0,
    max: 100,
    sufix: '%'
  },
  contrast: {
    label: 'Contrast',
    min: 0,
    max: 100,
    sufix: '%'
  },
  saturate: {
    label: 'Saturation',
    min: 0,
    max: 100,
    sufix: '%'
  },
};

export const defaultState: DefaultStateProps = {
  rotate: 0,
  grayscale: 0,
  blur: 0,
  brightness: 100,
  contrast: 100,
  saturate: 100
};