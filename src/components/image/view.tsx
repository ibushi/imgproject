import React, { useContext, useState, useEffect } from 'react';
import { Context } from '.';
import { slidersConfig } from './shared/config';
import { ImageViewProps } from './shared/types';


const classes = {
  imgContainer: { padding: '50px 0 70px 0' },
  img: { maxWidth: 500, },
  pixels: { padding: '70px 0 70px 50px' }
};

export default function ImageView({ src }: ImageViewProps): JSX.Element {
  const { filters }: any = useContext(Context);
  const [pixels, setPixels] = useState({ height: 0, width: 0 });

  useEffect(() => {
    if (!src) setPixels({ height: 0, width: 0 });
  }, [src]);

  return (
    <div className="row">
      <div className="col-md-auto" style={classes.imgContainer}>
        <img
          style={{
            ...classes.img,
            transform: `rotate(${filters.rotate}${slidersConfig.rotate.sufix})`,
            filter: Object.keys(filters)
              .filter(key => key !== 'rotate')
              .map((key) => `${key}(${filters[key]}${slidersConfig[key].sufix})`)
              .join(' '),
          }}
          src={src}
          onLoad={({ target: img }: any) =>
            setPixels({
              height: img.naturalHeight,
              width: img.naturalWidth
            })
          }
        />
      </div>
      {src &&
        <div className="col" style={classes.pixels}>
          <strong>Image height:</strong> {pixels.height}px<br />
          <strong>Image width:</strong> {pixels.width}px<br />
          <strong>Total no. of pixels:</strong> {pixels.height * pixels.width}px
        </div>
      }
    </div>
  );
}