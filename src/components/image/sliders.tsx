import React, { useContext } from 'react';
import { Slider } from '@blueprintjs/core';
import { slidersConfig } from './shared/config';
import { Context } from '.';
import { SliderItemProps, FiltersConfigProps } from './shared/types';

const classes = {
  slider: { marginBottom: '30px' }
};

export default function SlidersList(): JSX.Element {
  const { filters }: FiltersConfigProps = useContext(Context);
  return (
    <div className="conatainer">
      {
        Object.keys(filters)
          .map((key: any) => <SliderItem key={key} type={key} value={filters[key]} />)
      }
    </div>
  );
}

function SliderItem({ type, value }: SliderItemProps): JSX.Element {
  const { dispatch }: any = useContext(Context);
  return (
    <div className="row" style={classes.slider}>
      <div className="col-md-2">
        {slidersConfig[type].label}:
      </div>
      <div className="col">
        <Slider
          min={slidersConfig[type].min}
          max={slidersConfig[type].max}
          stepSize={1}
          labelStepSize={slidersConfig[type].labelStepSize || 10}
          onChange={(value: number) => dispatch({ type: 'CHANGE', payload: { type, value } })}
          value={value}
        />
      </div>
    </div>
  );
}